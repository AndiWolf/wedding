<?php

$GLOBALS['TCA']['sys_file_reference']['columns']['crop']['config'] = array(
    'type' => 'imageManipulation',
    'allowedExtensions' => 'jpg',
    'cropVariants' => [
        'desktop' => [
            'title' => 'Desktop',
            'allowedAspectRatios' => [
                'NaN' => [
                    'title' => 'Frei',
                    'value' => 0.0
                ],
                '16:9' => [
                    'title' => '16:9 Auflösung',
                    'value' => 16 / 9
                ],
            ]
        ],
        'tablet' => [
            'title' => 'Tablets',
            'allowedAspectRatios' => [
                'NaN' => [
                    'title' => 'Frei',
                    'value' => 0.0
                ],
                '16:9' => [
                    'title' => '16:9 Auflösung',
                    'value' => 16 / 9
                ]
            ]
        ],
        'mobile' => [
            'title' => 'Mobiltelefone',
            'allowedAspectRatios' => [
                'NaN' => [
                    'title' => 'Frei',
                    'value' => 0.0
                ],
                '16:9' => [
                    'title' => '16:9 Auflösung',
                    'value' => 16 / 9
                ]
            ]
        ]
    ]
);