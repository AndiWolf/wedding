<?php
defined('TYPO3_MODE') || die ('Access denied.');

// Enable RTE Preset
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets'][$_EXTKEY . '_default'] = 'EXT:' . $_EXTKEY . '/Configuration/RTE/Default.yaml';

// Settings default values for hidden fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('
TCAdefaults {
    tt_content {
        frame_class = none
    }
}
');