<?php
defined('TYPO3_MODE') || die ('Access denied.');

TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Provider-Extension: Hochzeit');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $_EXTKEY,
    'Configuration/tsconfig.tsconfig',
    'Provider-Extension: Hochzeit'
);