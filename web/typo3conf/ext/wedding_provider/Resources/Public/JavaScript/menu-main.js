$('#menuMainButton').on('click', function() {
    let $menuButton = $(this);
    let $menuMain = $('#menuMain');
    let classOpenMenuButton = 'menu-button--open';
    let classOpenMenu = 'menu--open';

    if ($menuButton.hasClass(classOpenMenuButton)) {
        $menuMain.removeClass(classOpenMenu);
        $menuButton.removeClass(classOpenMenuButton);
    } else {
        $menuMain.addClass(classOpenMenu);
        $menuButton.addClass(classOpenMenuButton);
    }
});