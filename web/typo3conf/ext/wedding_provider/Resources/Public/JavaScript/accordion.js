$('.accordion__button').on('click', function() {
    let $thisButton = $(this);
    let $listItem = $thisButton.closest('.accordion__list-item');
    let $listContent = $listItem.find('.accordion__content');
    let activeClass = 'accordion__button--active';

    if ($thisButton.hasClass(activeClass)) {
        $thisButton.removeClass(activeClass);
        $listContent.slideUp(300);
    } else {
        $thisButton.addClass(activeClass);
        $listContent.slideDown(300);
    }
});
