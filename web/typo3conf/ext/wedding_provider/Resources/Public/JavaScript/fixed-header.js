var debounce_timer;

$(window).scroll(function() {
    if(debounce_timer) {
        window.clearTimeout(debounce_timer);
    }

    debounce_timer = window.setTimeout(function() {
        fixHeader();
    }, 100);
});

$(function() {
    fixHeader();
});

function fixHeader() {
    let classScrolled = 'header--scrolled';
    if ($(window).scrollTop() > 0) {
        $('.header').addClass(classScrolled);
    } else {
        $('.header').removeClass(classScrolled);
    }
}