$(function() {
    $('#image-gallery').justifiedGallery({
        rowHeight : 300,
        lastRow : 'nojustify',
        margins : 3
    });
});
