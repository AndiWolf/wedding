<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Hochzeit Provider',
	'description' => '',
	'category' => 'misc',
	'shy' => 0,
	'version' => '1.0.0',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Andreas Wolf',
	'author_email' => 'kontakt@andi-wolf.de',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
            'typo3' => '9.5-9.5.99'
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:0:{}',
	'suggests' => array(
	),
);
