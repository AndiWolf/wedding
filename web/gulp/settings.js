module.exports = {
	dir: {
		extension: 'typo3conf/ext/wedding_provider/', // is always used as a prefix, so it includes the ending /
		public: 'Resources/Public',
		private: 'Resources/Private',
		scss: 'Resources/Private/Scss',
		css: 'Resources/Public/Css',
		js: 'Resources/Public/JavaScript',
		favicon: 'Resources/Public/Graphics/favicon'
	},
	favicon: {
		filename: 'favicon.png',
		dataFile: 'faviconData.json',
		company: '[COMPANYNAME]',
		companyColor: '#[HEX-CODE]',
		faviconBackground: '#[HEX-CODE]',
		silhouette: 'whiteSilhouette' // Choose between black and white (maybe even more) silhouettes - changes color of the icon itself
	},
	vagrant: {
		cmd: 'vagrant ssh -c ',
		cmd_postfix: ' -- -A'
	}
};
